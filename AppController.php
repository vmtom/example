<?php

namespace app\controllers;

use yii\web\Controller;

/**
 * Description of AppController
 *
 * @author tmolnar
 */
class AppController extends Controller {

    public function loadModel($className, $idOrModel = null, $idParamName = null) {
        if (!$idOrModel) {
            $idOrModel = filter_input(INPUT_GET, $idParamName, FILTER_VALIDATE_INT);
        }
        $qualifiedClassName = 'app\\models\\' . $className;
        if ($idOrModel instanceof $qualifiedClassName) {
            $model = $idOrModel;
        }
        elseif (!is_null($idOrModel)) {
            $model = $qualifiedClassName::findOne([
                'ID' => $idOrModel,
            ]);
        }
        else {
            $model = new $qualifiedClassName();
        }
        return $model;
    }

}
