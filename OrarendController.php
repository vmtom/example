<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\components\Utils;
use app\models\WpUser;
use app\models\MdszTanev;
use app\models\TmOsztalyTanmenet;
use app\models\TmOsztalyTanitasNelkuliNap;
use app\models\TmOrarend;

/**
 * Description of OrarendController
 *
 * @author vmtom
 */
class OrarendController extends AppController {

    public function actionIndex($TM_OSZTALY_TANMENET_ID) {
        $TmOsztalyTanmenet = $this->loadModel('TmOsztalyTanmenet', $TM_OSZTALY_TANMENET_ID);

        // órarend generálása, ha még nincs
        if (!$TmOsztalyTanmenet->hasOrarend()) {
            $TmOsztalyTanmenet->generateOrarend();
        }

        $TmOrarendek = TmOrarend::find()->joinWith(['tMFELHASZNALTORAK', 'tMFELHASZNALTORAK.tMTANMENETEVSZAK', 'tMFELHASZNALTORAK.tMTANMENETEVSZAK.tMKTTEGYSEG'])
                ->where(['TM_TANMENET_EVSZAK.TM_OSZTALY_TANMENET_ID' => $TmOsztalyTanmenet->ID])->orderBy('TM_ORAREND.TANITASI_NAP_DATUMA, TM_ORAREND.HANYADIK_ORA')->all();

        $orarend = [];
        foreach ($TmOrarendek as $TmOrarend) {
            if (!isset($orarend[$TmOrarend->TANITASI_NAP_DATUMA])) {
                $orarend[$TmOrarend->TANITASI_NAP_DATUMA] = [];
            }
            $orarend[$TmOrarend->TANITASI_NAP_DATUMA][] = $TmOrarend;
        }

        $TmOsztalyTanitasNelkuliNapok = TmOsztalyTanitasNelkuliNap::find()->where(['TM_OSZTALY_TANMENET_ID' => $TmOsztalyTanmenet->ID])
                ->orderBy('TANITAS_NELKULI_NAP_DATUMA')->indexBy('TANITAS_NELKULI_NAP_DATUMA')->all();

        return $this->renderAjax('index', [
            'TmOsztalyTanmenet' => $TmOsztalyTanmenet,
            'TmOsztalyTanitasNelkuliNapok' => $TmOsztalyTanitasNelkuliNapok,
            'orarend' => $orarend,
        ]);
    }

}
