<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\components\Utils;
use app\models\TmOsztalyTanmenet;
use app\models\TmFejlesztesiTerulet;
use app\models\TmTematikusTervOra;
use app\models\TmTematikusTervOraKttElem;
use app\models\TmFelhasznaltOrakIndikator;
use app\models\TmTematikusTervIndikator;
use app\models\TmTematikusTervErtekelesModszere;

/**
 * Description of OrarendController
 *
 * @author vmtom
 */
class TematikustervController extends AppController {

    public function actionSzerkesztes($ID) {
        $TmTematikusTerv = $this->loadModel('TmTematikusTerv', $ID);
        if (Yii::$app->request->isPost) {
            $postData = filter_input_array(INPUT_POST);
            $TmTematikusTerv->setAttributes($postData['TmTematikusTerv']);
            if ($TmTematikusTerv->save()) {
                // értékelés módszereinek mentése, megszűnők törlése
                $TmTematikusTervErtekelesModszerek = $TmTematikusTerv->getTmTematikusTervErtekelesModszeres()->indexBy('TM_ERTEKELES_MODSZERE_ID')->all();
                $ertekelesModszerIdk = array_keys($TmTematikusTervErtekelesModszerek);
                $ujErtekelesModszerIdk = isset($postData['TmTematikusTervErtekelesModszere']) ? array_values($postData['TmTematikusTervErtekelesModszere']) : [];
                $modszerIdsToInsert = array_diff($ujErtekelesModszerIdk, $ertekelesModszerIdk);
                $modszerIdsToDelete = array_diff($ertekelesModszerIdk, $ujErtekelesModszerIdk);
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    // törlések
                    foreach ($TmTematikusTervErtekelesModszerek as $TmTematikusTervErtekelesModszere) {
                        if (in_array($TmTematikusTervErtekelesModszere->TM_ERTEKELES_MODSZERE_ID, $modszerIdsToDelete)) {
                            $TmTematikusTervErtekelesModszere->delete();
                        }
                    }
                    // hozzáadások
                    foreach ($modszerIdsToInsert as $modszerId) {
                        $UjTmTematikusTervErtekelesModszere = new TmTematikusTervErtekelesModszere();
                        $UjTmTematikusTervErtekelesModszere->TM_TEMATIKUS_TERV_ID = $TmTematikusTerv->ID;
                        $UjTmTematikusTervErtekelesModszere->TM_ERTEKELES_MODSZERE_ID = $modszerId;
                        $UjTmTematikusTervErtekelesModszere->save();
                    }
                    // commit
                    $transaction->commit();
                }
                catch (\Throwable $ex) {
                    $transaction->rollBack();
                }
                return Yii::$app->utils->jsonResponse(['success' => true]);
            }
        }
        return $this->renderAjax('szerkesztes', [
            'model' => $TmTematikusTerv,
        ]);
    }

    public function actionFejlesztesiteruletek($TM_TEMATIKUS_TERV_ID) {
        $TmTematikusTerv = $this->loadModel('TmTematikusTerv', $TM_TEMATIKUS_TERV_ID);

        // ha a tematikus tervben csak 1 KTT egység van a szabad órakereten kívül, akkor az automatikusan legyen kiválasztva a szabad órákhoz
        $kotottFelhasznaltOrak = $TmTematikusTerv->getKotottFelhasznaltOrak();
        $szabadFelhasznaltOrak = $TmTematikusTerv->getSzabadFelhasznaltOrak();
        if (count($szabadFelhasznaltOrak) > 0 && count($kotottFelhasznaltOrak) === 1) {
            $kotottFelhasznaltOra = $kotottFelhasznaltOrak[0];
            foreach ($szabadFelhasznaltOrak as $szabadFelhasznaltOra) {
                for ($oraSorszam = 1; $oraSorszam <= $szabadFelhasznaltOra->FELHASZNALT_ORASZAM; ++$oraSorszam) {
                    $searchAttribs = ['TM_FELHASZNALT_ORAK_ID' => $szabadFelhasznaltOra->ID, 'ORA_SORSZAM' => $oraSorszam];
                    $TmTematikusTervOra = TmTematikusTervOra::find()->where($searchAttribs)->one();
                    if (!$TmTematikusTervOra) {
                        $TmTematikusTervOra = new TmTematikusTervOra();
                        $attributes = array_merge($searchAttribs, ['TM_SZABAD_FELHASZNALT_ORAK_ID' => $kotottFelhasznaltOra->ID]);
                        $TmTematikusTervOra->setAttributes($attributes);
                        $TmTematikusTervOra->save();
                    }
                }
            }
        }

        return $this->renderAjax('fejlesztesiteruletek', [
            'TmTematikusTerv' => $TmTematikusTerv,
        ]);
    }

    public function actionSzabadorakttmentes() {
        $postData = filter_input_array(INPUT_POST);
        $response = ['success' => false];

        $searchAttribs = [
            'TM_FELHASZNALT_ORAK_ID' => $postData['TM_FELHASZNALT_ORAK_ID'],
            'ORA_SORSZAM' => $postData['ORA_SORSZAM'],
        ];
        $TmTematikusTervOra = TmTematikusTervOra::find()->where($searchAttribs)->one();

        // mentés
        if (!empty($postData['TM_SZABAD_FELHASZNALT_ORAK_ID'])) {
            if (!$TmTematikusTervOra) {
                $TmTematikusTervOra = new TmTematikusTervOra();
                $TmTematikusTervOra->setAttributes($searchAttribs);
            }
            $TmTematikusTervOra->TM_SZABAD_FELHASZNALT_ORAK_ID = $postData['TM_SZABAD_FELHASZNALT_ORAK_ID'];
            $response['success'] = $TmTematikusTervOra->save();
        }
        // törlés
        else {
            if ($TmTematikusTervOra) {
                $deletedRowsCount = $TmTematikusTervOra->delete();
                $response['success'] = $deletedRowsCount > 0;
            }
        }

        return Yii::$app->utils->jsonResponse($response);
    }

    public function actionOrakttelemmentes() {
        $postData = filter_input_array(INPUT_POST);
        $response = ['success' => false];

        $searchAttribs = [
            'TM_FELHASZNALT_ORAK_ID' => $postData['TM_FELHASZNALT_ORAK_ID'],
            'ORA_SORSZAM' => $postData['ORA_SORSZAM'],
        ];
        $TmTematikusTervOra = TmTematikusTervOra::find()->where($searchAttribs)->one();

        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (!$TmTematikusTervOra) {
                $TmTematikusTervOra = new TmTematikusTervOra();
                $TmTematikusTervOra->setAttributes($searchAttribs);
                $TmTematikusTervOra->save();
            }
            $TmTematikusTervOraKttElem = new TmTematikusTervOraKttElem();
            $TmTematikusTervOraKttElem->TM_TEMATIKUS_TERV_ORA_ID = $TmTematikusTervOra->ID;
            $TmTematikusTervOraKttElem->TM_KTT_ELEM_ID = $postData['TM_KTT_ELEM_ID'];
            $TmTematikusTervOraKttElem->save();
            // commit
            $transaction->commit();
            $response['TM_TEMATIKUS_TERV_ORA_KTT_ELEM_ID'] = $TmTematikusTervOraKttElem->ID;
            $response['success'] = true;
        }
        catch (\Throwable $ex) {
            $transaction->rollBack();
        }

        return Yii::$app->utils->jsonResponse($response);
    }

    public function actionOratorles($TM_FELHASZNALT_ORAK_ID, $ORA_SORSZAM) {
        $response = ['success' => false];
        $TmTematikusTervOra = TmTematikusTervOra::find()->where(['TM_FELHASZNALT_ORAK_ID' => $TM_FELHASZNALT_ORAK_ID, 'ORA_SORSZAM' => $ORA_SORSZAM])->one();
        if ($TmTematikusTervOra) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                TmTematikusTervOraKttElem::deleteAll(['TM_TEMATIKUS_TERV_ORA_ID' => $TmTematikusTervOra->ID]);
                $TmTematikusTervOra->delete();
                // commit
                $transaction->commit();
                $response['success'] = true;
            }
            catch (\Throwable $ex) {
                $transaction->rollBack();
            }
        }
        return Yii::$app->utils->jsonResponse($response);
    }

    public function actionOrakttelemtorles($TM_TEMATIKUS_TERV_ORA_KTT_ELEM_ID) {
        $response = ['success' => false];
        $TmTematikusTervOraKttElem = $this->loadModel('TmTematikusTervOraKttElem', $TM_TEMATIKUS_TERV_ORA_KTT_ELEM_ID);
        if ($TmTematikusTervOraKttElem) {
            $TmTematikusTervOra = $TmTematikusTervOraKttElem->tMTEMATIKUSTERVORA;
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $TmTematikusTervOraKttElem->delete();
                if (TmTematikusTervOraKttElem::find()->where(['TM_TEMATIKUS_TERV_ORA_ID' => $TmTematikusTervOra->ID])->count() == 0) {
                    $TmTematikusTervOra->delete();
                }
                // commit
                $transaction->commit();
                $response['success'] = true;
            }
            catch (\Throwable $ex) {
                $transaction->rollBack();
            }
        }
        return Yii::$app->utils->jsonResponse($response);
    }

    public function actionF1indikatormentes() {
        $postData = filter_input_array(INPUT_POST);
        $response = ['success' => false];

        $searchAttribs = $postData['TmFelhasznaltOrakIndikator'];
        $TmFelhasznaltOrakIndikator = TmFelhasznaltOrakIndikator::find()->where($searchAttribs)->one();

        // indikátor hozzáadása
        if ($postData['value'] && !$TmFelhasznaltOrakIndikator) {
            $TmFelhasznaltOrakIndikator = new TmFelhasznaltOrakIndikator();
            $TmFelhasznaltOrakIndikator->setAttributes($searchAttribs);
            $response['success'] = $TmFelhasznaltOrakIndikator->save();
        }
        // indikátor törlése
        elseif (!$postData['value'] && $TmFelhasznaltOrakIndikator) {
            $deletedRowsCount = $TmFelhasznaltOrakIndikator->delete();
            $response['success'] = $deletedRowsCount > 0;
        }
        
        return Yii::$app->utils->jsonResponse($response);
    }

    public function actionF2f5indikatormentes() {
        $postData = filter_input_array(INPUT_POST);
        $response = ['success' => false];

        $searchAttribs = $postData['TmTematikusTervIndikator'];
        $TmTematikusTervIndikator = TmTematikusTervIndikator::find()->where($searchAttribs)->one();

        // indikátor hozzáadása
        if ($postData['value'] && !$TmTematikusTervIndikator) {
            $TmTematikusTervIndikator = new TmTematikusTervIndikator();
            $TmTematikusTervIndikator->setAttributes($searchAttribs);
            $response['success'] = $TmTematikusTervIndikator->save();
        }
        // indikátor törlése
        elseif (!$postData['value'] && $TmTematikusTervIndikator) {
            $deletedRowsCount = $TmTematikusTervIndikator->delete();
            $response['success'] = $deletedRowsCount > 0;
        }
        
        return Yii::$app->utils->jsonResponse($response);
    }

}
