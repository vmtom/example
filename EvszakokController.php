<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\components\Utils;
use app\models\TmOsztalyTanmenet;
use app\models\TmEvfolyamCsoport;
use app\models\TmTanmenetEvszak;
use app\models\TmTematikusTerv;
use app\models\TmFelhasznaltOrak;
use app\models\TmKerettanterviTartalmiEgyseg;
use app\models\TmTematikusTervOra;
use app\models\TmTematikusTervOraKttElem;

/**
 * Description of EvszakokController
 *
 * @author tmolnar
 */
class EvszakokController extends AppController {

    public function actionIndex() {
        $request = filter_input_array(INPUT_GET);
        $TmOsztalyTanmenet = TmOsztalyTanmenet::findOne(['ID' => $request['TM_OSZTALY_TANMENET_ID']]);
        return $this->renderAjax('index', [
            'TmOsztalyTanmenet' => $TmOsztalyTanmenet,
            'maradekEgysegekSzama' => count($TmOsztalyTanmenet->getEvszakhozNemRendeltEgysegek()),
        ]);
    }

    public function actionKttLista() {
        $request = filter_input_array(INPUT_GET);
        $TmOsztalyTanmenet = TmOsztalyTanmenet::findOne(['ID' => $request['TM_OSZTALY_TANMENET_ID']]);
        if ($TmOsztalyTanmenet) {
            return $this->renderAjax('kttlista', [
                'TmOsztalyTanmenet' => $TmOsztalyTanmenet,
            ]);
        }
    }

    public function actionEvszaklista() {
        $request = filter_input_array(INPUT_GET);
        $TmOsztalyTanmenet = TmOsztalyTanmenet::findOne(['ID' => $request['TM_OSZTALY_TANMENET_ID']]);
        if ($TmOsztalyTanmenet) {
            return $this->renderAjax('evszaklista', [
                'TmOsztalyTanmenet' => $TmOsztalyTanmenet,
                'EVSZAK_KOD' => $request['EVSZAK_KOD'],
            ]);
        }
    }

    public function actionTervek($TM_OSZTALY_TANMENET_ID, $TM_TEMATIKUS_TERV_ID = null) {
        $TmOsztalyTanmenet = TmOsztalyTanmenet::findOne(['ID' => $TM_OSZTALY_TANMENET_ID]);
        return $this->renderAjax('tervek', [
            'TmOsztalyTanmenet' => $TmOsztalyTanmenet,
            'TM_TEMATIKUS_TERV_ID' => $TM_TEMATIKUS_TERV_ID,
        ]);
    }

    public function actionTervlista() {
        $request = filter_input_array(INPUT_GET);
        $TmOsztalyTanmenet = TmOsztalyTanmenet::findOne(['ID' => $request['TM_OSZTALY_TANMENET_ID']]);
        if ($TmOsztalyTanmenet) {
            return $this->renderAjax('tervlista', [
                'TmOsztalyTanmenet' => $TmOsztalyTanmenet,
            ]);
        }
    }

    public function actionUjterv($TM_OSZTALY_TANMENET_ID) {
        $utolsoTerv = TmTematikusTerv::find()->where(['TM_OSZTALY_TANMENET_ID' => $TM_OSZTALY_TANMENET_ID])->orderBy('SORSZAM DESC')->one();
        $TmTematikusTerv = new TmTematikusTerv();
        $TmTematikusTerv->TM_OSZTALY_TANMENET_ID = $TM_OSZTALY_TANMENET_ID;
        $TmTematikusTerv->SORSZAM = $utolsoTerv ? $utolsoTerv->SORSZAM + 1 : 1;
        if ($TmTematikusTerv->save()) {
            return $this->actionTervdoboz($TmTematikusTerv->ID);
        }
    }

    public function actionTervtorles($TM_TEMATIKUS_TERV_ID) {
        $response = ['success' => false];
        $TmTematikusTerv = $this->loadModel('TmTematikusTerv', $TM_TEMATIKUS_TERV_ID);
        if ($TmTematikusTerv) {
            $TmOsztalyTanmenet = $TmTematikusTerv->tMOSZTALYTANMENET;
            // esetleges órarend törlése
            $TmOsztalyTanmenet->deleteOrarend();
            // tematikus terv törlése
            $TmTematikusTerv->delete();
            // tervek újrasorszámozása
            $tobbiTerv = TmTematikusTerv::find()->where(['TM_OSZTALY_TANMENET_ID' => $TmOsztalyTanmenet->ID])->orderBy('SORSZAM ASC')->all();
            foreach ($tobbiTerv as $index => $terv) {
                $terv->SORSZAM = $index + 1;
                $terv->save(false);
            }
            $response['success'] = true;
            $response['validationResult'] = $TmOsztalyTanmenet->validateTematikusTervek();
        }
        return Yii::$app->utils->jsonResponse($response);
    }

    public function actionTervdoboz($TM_TEMATIKUS_TERV_ID = null) {
        $TmTematikusTerv = $this->loadModel('TmTematikusTerv', $TM_TEMATIKUS_TERV_ID);
        if ($TmTematikusTerv) {
            return $this->renderAjax('tervdoboz', [
                'TmTematikusTerv' => $TmTematikusTerv,
            ]);
        }
    }

    public function actionKttmentestervhez() {
        $request = filter_input_array(INPUT_POST);
        $response = ['success' => false];

        $TmTanmenetEvszak = TmTanmenetEvszak::find()->with('tMOSZTALYTANMENET')->where([
            'ID' => $request['TM_TANMENET_EVSZAK_ID'],
        ])->one();

        if ($TmTanmenetEvszak) {
            // esetleges órarend törlése
            $TmTanmenetEvszak->tMOSZTALYTANMENET->deleteOrarend();

            $searchAttribs = [
                'TM_TANMENET_EVSZAK_ID' => $TmTanmenetEvszak->ID,
                'TM_TEMATIKUS_TERV_ID' => $request['TM_TEMATIKUS_TERV_ID'],
            ];
            $TmFelhasznaltOrak = TmFelhasznaltOrak::find()->where($searchAttribs)->one();
            $add = $request['value'] === '1';
            // tervhez kötés
            if ($add) {
                if (!$TmFelhasznaltOrak) {
                    $TmFelhasznaltOrak = new TmFelhasznaltOrak();
                    $TmFelhasznaltOrak->setAttributes($searchAttribs);
                }
                $TmFelhasznaltOrak->FELHASZNALT_ORASZAM = $TmTanmenetEvszak->tMKTTEGYSEG->SZABADON_FELHASZNALHATO_ORAKERET
                        ? $request['FELHASZNALT_ORASZAM']
                        : $TmTanmenetEvszak->RAFORDITOTT_ORASZAM;
                $TmFelhasznaltOrak->save();
            }
            // kötés megszüntetése
            else {
                if ($TmFelhasznaltOrak) {
                    $TmFelhasznaltOrak->delete();
                }
            }
            $response['success'] = true;
            $response['validationResult'] = $TmTanmenetEvszak->tMOSZTALYTANMENET->validateTematikusTervek();
        }
        
        return Yii::$app->utils->jsonResponse($response);
    }

    public function actionKttidk($TM_OSZTALY_TANMENET_ID) {
        $response = null;
        $TmOsztalyTanmenet = $this->loadModel('TmOsztalyTanmenet', $TM_OSZTALY_TANMENET_ID);
        if ($TmOsztalyTanmenet) {
            $response = $TmOsztalyTanmenet->getKttEgysegIdk();
        }
        return Yii::$app->utils->jsonResponse($response);
    }

    public function actionTervmegnevezes($ID) {
        $TmTematikusTerv = $this->loadModel('TmTematikusTerv', $ID);
        if (Yii::$app->request->isPost) {
            $postData = filter_input_array(INPUT_POST);
            $TmTematikusTerv->setAttributes($postData['TmTematikusTerv']);
            if ($TmTematikusTerv->save()) {
                return Yii::$app->utils->jsonResponse(['success' => true]);
            }
        }
    }

}
